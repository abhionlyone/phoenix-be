Rails.application.routes.draw do
  resources :companies do
    resources :founders, only: [:index, :create]
  end
end
