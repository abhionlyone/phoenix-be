class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :company_name
      t.string :city
      t.string :state
      t.date :founded_date
      t.text :description

      t.timestamps
    end
  end
end
