if Rails.env.development?
  100.times do
    Company.create(company_name: Faker::Company.name, 
      state: Faker::Address.state, city: Faker::Address.city, 
      founded_date: Time.at( rand * (Time.now.to_f - 0.to_f)), 
      description: Faker::Lorem.paragraph)
  end
end