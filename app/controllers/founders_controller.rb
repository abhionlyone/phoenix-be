class FoundersController < ApplicationController
  before_action :set_founder, only: [:show, :update, :destroy]
  before_action :set_company

  # GET /founders
  def index
    @founders = @company.founders
    render json: @founders
  end

  # POST /founders
  def create
    @founder = Founder.new(founder_params)

    if @founder.save
      render json: @founder, status: :created
    else
      render json: @founder.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_founder
      @founder = Founder.find(params[:id])
    end

    def set_company
      @company = Company.find(params[:company_id])
    end

    # Only allow a trusted parameter "white list" through.
    def founder_params
      params.require(:founder).permit(:name, :title, :company_id)
    end
end
